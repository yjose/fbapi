@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="form-group">
    {!! Form::label('name','Page Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'your page name']) !!}
</div>
<div class="form-group">
    {!! Form::label('page_id', 'page Id:') !!}
    {!! Form::text('page_id', null, ['class' => 'form-control','placeholder'=>'your page id']) !!}
</div>


{!!Form::submit($text,['class'=>'btn btn-info pull-right '])!!}