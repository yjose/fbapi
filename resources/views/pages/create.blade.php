@extends('layouts.app2')
@section('content')
    <div class="row panel panel-footer">
        <br/>
        <div class="col-md-12 pull-center">
            {!! Form::model(new \App\Page,['route' => 'pages.store']) !!}
            @include('pages/_form', ['text' => 'Create Page'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection