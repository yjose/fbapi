@extends('layouts.app2')

@section('content')
    <div class="row">

        <div class="x_panel">
            <div class="x_title">
                <h2> All Pages </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>page id</th>
                        <th>page Access Token </th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $page)
                        <tr>
                            <th scope="row">{{$page->id}}</th>
                            <td>{{$page->name}}</td>
                            <td>{{$page->page_id}}</td>
                            <td>

                            </td>
                            <td width="180px">

                                <a href="{{url("/pages/$page->id/edit")}}" class="left"><span class=" fa fa-3x fa-edit  "></span></a>
                                {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('pages.destroy', $page->id))) !!}
                                <button type="submit" class="fa fa-2x fa-remove btn-danger"></button>
                                {!! Form::close() !!}


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection