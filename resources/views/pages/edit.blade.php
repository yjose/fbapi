@extends('layouts.app2')

@section('content')
    <div class="row panel panel-footer">
        <br/>
        <div class="col-md-12 pull-center">
            {!! Form::model($page,['method'=>'put' , 'url'=>route('pages.update',$page)]) !!}
            @include('pages/_form', ['text' => 'update Page'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection