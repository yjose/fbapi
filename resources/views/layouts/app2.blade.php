<!DOCTYPE html>
<html lang="en" data-ng-app="fbapiApp">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title data-ng-bind="'FbAPI ADMIN!| ' + $state.current.data.pageTitle"></title>
    <!-- Bootstrap core CSS -->

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('fonts/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/toaster.min.css')}}" rel="stylesheet">

    <link id="ng_load_plugins_before" />
    @yield('css')
    @yield('script')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">
<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ url('/')}}" class="site_title"><i class="fa fa-facebook-square"></i> <span>FbAPI Admin!</span></a>
                </div>
                <div class="clearfix"></div>

                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>General </h3>
                        <ul class="nav side-menu">
                            <li><a style="cursor: pointer"><i class="fa fa-edit"></i>Tools <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" >
                                    <li><a ui-sref="apps">facebook apps</a>
                                    </li>
                                    <li><a ui-sref="pages">facebook pages</a>
                                    </li>
                                    <li><a ui-sref="access_tokens">facebook Access Tokens</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->
                <div class="clearfix"></div>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>Scraping section </h3>
                        <ul class="nav side-menu">
                            <li><a style="cursor: pointer"><i class="fa fa-home"></i> Scraping <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><a href="#">Add page</a>
                                    </li>
                                    <li><a ui-sref="scraping">Scraping</a>
                                    </li>
                                    <li><a href="#">????</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a style="cursor: pointer"><i class="fa fa-edit"></i> Posting <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" >
                                    <li><a ui-sref="posts">Posts</a>
                                    </li>
                                    <li><a ui-sref="tasks">Tasks</a>
                                    </li>
                                    <li><a href="#">F???</a>
                                    </li>
                                </ul>
                            </li>


                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->



            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> Log Out </a>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col " role="main">
            <toaster-container  toaster-options="{'close-button': true}"></toaster-container>

            @yield('content')

            <!-- footer content -->

            <footer>
                <div class=" pull-right">
                    <span class="lead"> <i class="fa fa-facebook-square"></i> FbApi Admin !</span>
                    </p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
        <!-- /page content -->

    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>


<script type="text/javascript" src="{{asset('js/lib/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lib/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lib/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lib/angular.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lib/toaster.min.js')}}" ></script>

<script type="text/javascript" src="{{asset('js/lib/modules/angular-ui-router.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lib/modules/angular-resource.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lib/modules/angular-animate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lib/modules/ocLazyLoad.min.js')}}"></script>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('js/services.js')}}"></script>
<script type="text/javascript" src="{{asset('js/directives.js')}}"></script>
<script type="text/javascript" src="{{asset('js/filters.js')}}"></script>




</body>

</html>
