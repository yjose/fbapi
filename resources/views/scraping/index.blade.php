@extends('layouts.app2')
@section('css')
    <link href="{{asset('css/object-table-style.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/themes/blue-dust.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="row" ng-app="App">
        <div class="x_panel">
            <div class="x_title">
                <h2> All Apps </h2>

                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div ng-controller="TableCtrl">
                    <div class="row ">
                        <button class="btn btn-primary btn-lg btn-block" ng-disabled="load" ng-click="startScrap()">Start Magic Scraping</button>
                    </div>
                    <div class="clearfix"></div>
                    <div ng-show="error" class="alert alert-danger " role="alert">
                          <p>  @{{error}} </p>
                    </div>
                    <div class="row" style="overflow: scroll" ng-show="success">
                        <table object-table class="blue-dust"
                               data="data"
                               display="20"
                               headers="Page,Type,Message,Link,Likes,Comments,Shares,created_time,Action"
                               fields="page,type,message,link,likes,comments,shares,created_time"
                               sorting="compound"
                               search="separate"
                                resize="true">
                            <tbody>
                            <tr>
                                <td>@{{::item.page}}</td>
                                <td>@{{::item.type}}</td>
                                <td>@{{::item.message}}</td>
                                <td>
                                    <a href="@{{::item.link}}"> get it </a>
                                </td>
                                <td>@{{::item.likes}}</td>
                                <td>@{{::item.comments}}</td>
                                <td>@{{::item.shares}}</td>
                                <td>@{{::item.created_time}}</td>
                                <td>
                                    <button class="fa fa-star fa-2x yellow-gold" ng-click="$owner.getItemValue(item)"></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="row" ng-show="load">
                        <img src="img/loading.gif" class="center-block" alt=""/>
                        <p class="text-center text-info">Loading data from facebook .. this process will take a few minutes thank</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/lib/angular.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/lib/object-table.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
@endsection