@extends('layouts.app2')

@section('content')
    <div class="row">

        <div class="x_panel">
            <div class="x_title">
                <h2>All Access Tokens </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>title</th>
                        <th>text</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tokens as $token)
                    <tr>
                        <th scope="row">{{$token->id}}</th>
                        <td>{{$token->name}}</td>
                        <td style="max-width: 300px;overflow: scroll"><span>{{$token->text}}</span></td>
                        <td width="180px">

                            <a href="{{url("/access_tokens/$token->id/edit")}}" class="left"><span class=" fa fa-3x fa-edit  "></span></a>
                                {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('access_tokens.destroy', $token->id))) !!}
                                <button type="submit" class="fa fa-2x fa-remove btn-danger"></button>
                                {!! Form::close() !!}


                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection