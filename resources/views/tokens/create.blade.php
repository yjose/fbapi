@extends('layouts.app2')

@section('content')
    <div class="row panel panel-footer">
        <br/>
        <div class="col-md-12 pull-center">
        {!! Form::model(new \App\Token,['route' => 'access_tokens.store']) !!}
            @include('tokens/_form', ['text' => 'Create Token'])
        {!! Form::close() !!}
    </div>
    </div>
@endsection