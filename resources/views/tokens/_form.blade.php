@if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="form-group">
    {!! Form::label('name','Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'access Token Title']) !!}
</div>
<div class="form-group">
    {!! Form::label('text', 'Access Token:') !!}
    {!! Form::text('text', null, ['class' => 'form-control','placeholder'=>'access Token Text']) !!}
</div>
<div class="form-group">
    {!! Form::label('Facebook App', 'Facebook App:') !!}
    {!! Form::select('app_id', \App\FApp::lists('name','id'), null, ['class'=>'form-control'])!!}
</div>
    <div class="form-group">
        {!! Form::label('Facebook page', 'Facebook Page:') !!}
        {!! Form::select('page_id', \App\Page::lists('name','id'), null, ['class'=>'form-control','placeholder'=>'token to scrap'])!!}
    </div>

{!!Form::submit($text,['class'=>'btn btn-info pull-right '])!!}