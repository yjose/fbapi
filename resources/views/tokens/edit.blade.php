@extends('layouts.app2')

@section('content')
    <div class="row panel panel-footer">
        <br/>
        <div class="col-md-12 pull-center">
            {!! Form::model($token,['method'=>'put' , 'url'=>route('access_tokens.update',$token)]) !!}
            @include('tokens/_form', ['text' => 'update Token'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection