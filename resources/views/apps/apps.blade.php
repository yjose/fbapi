@extends('layouts.app2')

@section('content')
    <div class="row">

        <div class="x_panel">
            <div class="x_title">
                <h2> All Apps </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>APP id</th>
                        <th>Secret App id</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($apps as $app)
                        <tr>
                            <th scope="row">{{$app->id}}</th>
                            <td>{{$app->name}}</td>
                            <td>{{$app->app_id}}</td>
                            <td>
                                {{$app->secret_app_id}}
                            </td>
                            <td width="180px">

                                <a href="{{url("/apps/$app->id/edit")}}" class="left"><span class=" fa fa-3x fa-edit  "></span></a>
                                {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('apps.destroy', $app->id))) !!}
                                <button type="submit" class="fa fa-2x fa-remove btn-danger"></button>
                                {!! Form::close() !!}


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection