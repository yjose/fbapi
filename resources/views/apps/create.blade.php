@extends('layouts.app2')

@section('content')
    <div class="row panel panel-footer">
        <br/>
        <div class="col-md-12 pull-center">
            {!! Form::model(new \App\FApp,['route' => 'apps.store']) !!}
            @include('apps/_form', ['text' => 'Create App'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection