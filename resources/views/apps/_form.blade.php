@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="form-group">
    {!! Form::label('name','App Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'page name']) !!}
</div>
<div class="form-group">
    {!! Form::label('app_id', 'App id:') !!}
    {!! Form::text('app_id', null, ['class' => 'form-control','placeholder'=>'you app id']) !!}
</div>
<div class="form-group">
    {!! Form::label('Secret app id', 'Secret App Id :') !!}
    {!! Form::text('secret_app_id', null, ['class'=>'form-control','placeholder'=>'you app id'])!!}
</div>

{!!Form::submit($text,['class'=>'btn btn-info pull-right '])!!}