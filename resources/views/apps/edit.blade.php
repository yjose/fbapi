@extends('layouts.app2')

@section('content')
    <div class="row panel panel-footer">
        <br/>
        <div class="col-md-12 pull-center">
            {!! Form::model($app,['method'=>'put' , 'url'=>route('apps.update',$app)]) !!}
            @include('apps/_form', ['text' => 'update App'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection