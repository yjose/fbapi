<!DOCTYPE html>
<head>
    <meta charset="utf-8"/>
    <title>FbAPI Admin | Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>


    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('css/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/components.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/layout.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/login-2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class=" login">
<div class="logo">
        <h1 class="text-logo">FbAPI</h1>
</div>

<div class="content">
    <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
        {!! csrf_field() !!}

        <div class="form-title">
            <span class="form-title">Welcome</span>
            <span class="form-subtitle">..</span>
            <hr/>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} ">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="email" type="email"
                   name="email" autocomplete="off" placeholder="Email"/>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                    </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
                   placeholder="Password" name="password"/>
            @if ($errors->has('password'))
                <span class="help-block">
                     <strong>{{ $errors->first('password') }}</strong>
                    </span>
            @endif
        </div>
        <div class="form-actions">
            <button type="submit" class="btn red btn-block uppercase">Login</button>
        </div>

        <div class="form-actions">
            <div class="pull-left">
                <label class="rememberme check">
                    <input type="checkbox" name="remember" value="1"/>Remember me </label>
            </div>
            <div class="pull-right forget-password-block">
                <a href="{{ url('/password/reset') }}" id="forget-password" class="forget-password">Forgot Password?</a>
            </div>
        </div>
        <hr/>

    </form>



</div>
<div class="copyright hide"> 2016 © FbAPI.</div>

<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
