<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected  $table="posts";
    protected $fillable = [
         'page_id','type','message','source','description','link','status','success_id','time_to_post'
    ];
    // status = 0 ==> add to favor
    //status = 1 ==> to post
    //status = 2 ==>posted success
    //status = 3 ==>posted error 1
    // status =4 ==> posted error 2
    public function page()
    {
        return $this->belongsTo('\App\Page');
    }
    public function scopeWithstatus($query,$status){
        return $query->where('status','=',$status);
    }
    public function scopeEventformat($query,$status=null){
        if($status==null)
           return \App\FbApi\Post::EventFormat(Post::all());
        else
            return \App\FbApi\Post::EventFormat(Post::withstatus($status)->get());

    }

}
