<?php
/**
 * Created by PhpStorm.
 * User: youssef
 * Date: 3/6/2016
 * Time: 20:13
 */

namespace App\FbApi;


class EventModel {
    public $id;
    public $start;
    public $background;
    public $allDay=false;
    const PHOTO = '#03A9F4';
    const VIDEO = '#00C34A';
    const LINK = '#F44336';
    const MESSAGE = '#673AB7';

    function __construct($id, $start,$background)
    {
        $this->id = $id;
        $this->start = $start;
        $this->background = $background;
        $this->allDay = false;
    }


}