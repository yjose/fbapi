<?php

namespace App\FbApi;
use App\Token;
use Facebook\Facebook;
use Intervention\Image\Facades\Image;

class Fb
{
    private static $fb = null;

    public static function getInstance()
    {
        return self::$fb;
    }

    public static function getNewInstance()
    {
        $session = self::getFbSessionFromDB();
        $fb = app(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
        self::$fb = $fb->newInstance([
            'app_id' => $session['app_id'],
            'app_secret' => $session['secret_app_id'],
            'default_graph_version' => 'v2.5',
        ]);
        self::$fb->setDefaultAccessToken($session['access_token']);
        return self::$fb;
    }

    public static function getFbSessionFromDB()
    {

        $tokens = Token::all()->where('page_id', 0)->shuffle();
        $token = $tokens[0];
        $session = [
            'app_id' => $token->app->app_id,
            'secret_app_id' => $token->app->secret_app_id,
            'access_token' => $token->text
        ];
        return $session;
    }


    public static function loadResources($post)
    {

        set_time_limit(600);
        if($post->type=='photo'){
            $img = Image::make($post->source);
            $path='facebook/images/photo__'.$post->id.'.jpg';
            $img->save($path);
            $post->status=2;
            $post->save();
        }
        if($post->type=='video'){
            $path='facebook/videos/video__'.$post->id.'.mp4';
            file_put_contents($path,file_get_contents($post->source));
            $post->status=2;
            $post->save();
        }

    }

}
