<?php

namespace App\FbApi;

use App\Page;
use Facebook\Facebook;
use Illuminate\Database\Eloquent\Collection;

class Post
{

    public static function getAllPosts($nbr_for_page)
    {
        $posts = [];
        $pages = Page::toscrap()->get();
        foreach ($pages as $page) {
            $posts = array_merge($posts, self::geAllPostFromPage($page->name,$nbr_for_page));
        }
        return $posts;
    }


    public static function geAllPostFromPage($page_id, $nbr_post = 10)
    {
        $fb = Fb::getNewInstance();
        $query = $page_id . '/posts?fields=type,source,full_picture,description,message,created_time,link,shares,likes.summary(1).limit(0),comments.summary(1).limit(0)&&limit=' . $nbr_post;
        try {
            $posts_request = $fb->get($query);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            return $e;
        }
        $posts_response = $posts_request->getGraphEdge();
        return Post::formatData($posts_response,$page_id);
    }

    public static function formatData($data,$page_id)
    {
        $posts = [];
        foreach ($data as $post) {
            $postModel = new PostModel();
            foreach ($post as $k => $value) {
                if ($k == 'likes' || $k == 'comments') {
                    $postModel->$k = $value->getTotalCount();
                } else if ($k == 'created_time') {
                    $postModel->$k = date_format($value, 'Y-m-d H:i:s');

                } else if ($k == 'shares') {
                    $postModel->$k = $value->getField('count');
                } else {
                    $postModel->$k = $value;
                }

                if($k=='type' && $value=='photo'){
                    $postModel->source=$post['full_picture'];
                }

            }
            $postModel->page=$page_id;
            array_push($posts, $postModel);
        }
        return $posts;
    }

    public static function  EventFormat($data){
        $events=array();
        foreach ($data as $post) {
            $type=$post->type;
            if($type=='video')
                $post->background=EventModel::VIDEO;
                //$event = new EventModel($post->id,$post->time_to_post,EventModel::VIDEO);
            else if($type == 'photo')
                $post->background=EventModel::PHOTO;
                //$event = new EventModel($post->id,$post->time_to_post,EventModel::PHOTO);
            else if($type=='link')
                $post->background=EventModel::LINK;
                //$event = new EventModel($post->id,$post->time_to_post,EventModel::LINK);
            else
                $post->background=EventModel::MESSAGE;
                //$event = new EventModel($post->id,$post->time_to_post,EventModel::MESSAGE);
           array_push($events, $post);
        }
       return $events;
    }
}
