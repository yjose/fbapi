<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected  $table="pages";
    protected $fillable = [
        'name', 'page_id','to_scrap','rate','nbr_errors'
    ];

    public function tokens()
    {
        return $this->hasMany('\App\Token');
    }
    public function scopeToScrap($query){
        return $query->where('to_scrap','=',1);
    }

    public function scopeToPost($query){
        return $query->where('to_scrap','=',0);
    }

}
