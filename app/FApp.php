<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FApp extends Model
{
    protected  $table="apps";
    protected $fillable = [
        'name', 'app_id', 'secret_app_id',
    ];

    public function tokens()
    {
        return $this->hasMany('\App\Token');
    }
}
