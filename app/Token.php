<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected  $table="tokens";
    protected $fillable = [
        'name', 'text', 'app_id','page_id',
    ];

    public function page()
    {
        return $this->belongsTo('\App\Page');
    }
    public function app()
    {
        return $this->belongsTo('\App\FApp');
    }

    public function scopeToscrap($query){
        return $query->where('page_id','0');
    }
}
