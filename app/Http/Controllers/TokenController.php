<?php

namespace App\Http\Controllers;


use App\Token;

use App\Http\Requests\TokenRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class TokenController extends Controller
{


    public function index()
    {
        $tokens = Token::all();
        return $tokens;
    }
    public function store(TokenRequest $token)
    {
            $t=Token::create($token->all());
            return response()->json($t);
    }

    public function update($id,TokenRequest $t)
    {
        $token= Token::findOrfail($id);
        $token->update($t->all());
    }

    public function destroy($id)
    {
        Token::destroy($id);
    }

}
