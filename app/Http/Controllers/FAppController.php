<?php

namespace App\Http\Controllers;

use App\FApp;

use App\Http\Requests\FAppRequest;
use App\Http\Controllers\Controller;

class FAppController extends Controller
{


    public function index()
    {
        $apps = FApp::all();
        return $apps;
    }
    public function show($id){
        $app= FApp::findOrfail($id);
        return $app;
    }
    public function store(FAppRequest $FApp)
    {
        $t=FApp::create($FApp->all());
        return response()->json($t);
    }

    public function update($id,FAppRequest $t)
    {
        $app= FApp::findOrfail($id);
        $app->update($t->all());
    }

    public function destroy($id)
    {
        FApp::destroy($id);
    }

    
    
}  
 /*

    public function index()
    {
        $apps = FApp::all();
        if (request()->isXmlHttpRequest()) {
            return $apps;
        }
        return $apps; //view('apps.apps',compact('apps'));
    }

    public function create()
    {
        return view('apps.create');
    }

    public function edit($id)
    {
        $app= FApp::findorfail($id);
        return view('apps.edit',compact('app'));
    }
    public function store(FAppRequest $FApp)
    {
        FApp::create( $FApp->all());
        return redirect(route('apps.index'));
    }


    public function update($id,FAppRequest $p)
    {
        $FApp= FApp::findOrfail($id);
        $FApp->update($p->all());
        return redirect(route('apps.index'));
    }

    public function destroy($id)
    {
        FApp::findOrfail($id)->delete();
        return redirect(route('apps.index'));
    }

}
*/