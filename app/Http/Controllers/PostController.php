<?php

namespace App\Http\Controllers;

use App\Post;

use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;

class PostController extends Controller
{

    public function index()
    {

        if(isset($_GET['format']) && isset($_GET['status'])){
                $status =$_GET['status'];
            //dd(Post::eventFormat($status));
                return Post::eventFormat($status);

        }
        if(isset($_GET['format'])){
            //dd(Post::eventFormat($status));
            return Post::eventFormat();

        }
        if(isset($_GET['status'])){
            $status =$_GET['status'];
            return Post::withstatus($status)->get();
        }
        return Post::All();

    }
    public function show($id){
        $page= Post::findOrfail($id);
        return $page;
    }
    public function store(PostRequest $Post)
    {
        $post=$Post->all();


        foreach($post as $key=>$value){
            if($value==null)
                $post[$key]="";
        }
        $p=Post::create($post);
        return response()->json($p);
    }

    public function update($id,PostRequest $p)
    {
        $page= Post::findOrfail($id);
        $page->update($p->all());
    }

    public function destroy($id)
    {
        Post::destroy($id);
    }


}