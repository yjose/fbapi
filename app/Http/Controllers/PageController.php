<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageRequest;
use App\Page;

use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index()
    {
        if(isset($_GET['to_post'])){
            return Page::toPost()->get();
        }

        $pages = Page::toScrap()->get();
        return $pages;
    }
    public function show($id){
        $page= Page::findOrfail($id);
        return $page;
    }
    public function store(PageRequest $Page)
    {
        $t=Page::create($Page->all());
        return response()->json($t);
    }

    public function update($id,PageRequest $t)
    {
        $page= Page::findOrfail($id);
        $page->update($t->all());
    }

    public function destroy($id)
    {
        Page::destroy($id);
    }

}
/*
    public function index()
    {

        $pages = Page::all();
        if (request()->isJson()) {
            return response()->json($pages);
        }
        return $pages;//view('pages.pages', compact('pages'));
    }

    public function create()
    {
        return view('pages.create');
    }

    public function edit($id)
    {
        $page = Page::findorfail($id);
        return view('pages.edit', compact('page'));
    }

    public function store(PageRequest $pager)
    {
        //dd($page->all());

        if ($pager->isJson()) {
                $p=Page::create($pager->all());
                return response()->json($p);

        }
        //return redirect(route('pages.index'));
    }
    public function show($id){
        try{
            $page = Page::findorfail($id);
        }catch (\Exception $e){
            return response()->json($e->getMessage(),200);
        }
        return $page;
    }


    public function update($id, PageRequest $p)
    {
        $page = Page::findOrfail($id);
        $page->update($p->all());
        if ($page->isJson()) {
            return response()->json($page);
        }
        return redirect(route('pages.index'));
    }

    public function destroy($id)
    {
        Page::findOrfail($id)->delete();
        return redirect(route('pages.index'));
    }

}
*/