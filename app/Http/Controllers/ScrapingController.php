<?php

namespace App\Http\Controllers;

use App\FbApi\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ScrapingController extends Controller
{

    public function index(){
        return view('scraping.index');
    }

    public function scrap(){
        $posts=[];
        try{
            $posts = Post::getAllPosts(10);
        }catch (\Exception $e){
            response()->json('ereur de Scraping'.$e,403);
        }
        return $posts;
    }
}
