<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::get('/test', function () {
    $post=\App\Post::findOrfail(29);
    var_dump($post);
    \App\FbApi\Fb::loadResources($post);
});


Route::group(['prefix' => 'api','middleware' => 'web'], function()
{
    Route::resource('/pages', 'PageController');
    Route::resource('/access_tokens', 'TokenController');
    Route::resource('/apps', 'FAppController');
    Route::resource('/posts', 'PostController');
    Route::get('/scrap','ScrapingController@scrap');
});

Route::group(['middleware' => 'web'], function () {

    Route::auth();
    //Route::get('/home', 'HomeController@index');
    Route::get('/', 'HomeController@index');

    Route::resource('/access_tokens', 'TokenController');

    Route::resource('/apps', 'FAppController');
    Route::resource('/posts', 'PostController');
    Route::get('/scraping','ScrapingController@index');



});

