/**
 * Created by youssef on 3/5/2016.
 */
fbapiApp.factory('Token', function ($resource) {
        return $resource('http://localhost/projects/fbapi/public/api/access_tokens/:id', {id: '@_id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })

    .factory('Page', function ($resource) {
        return $resource('http://localhost/projects/fbapi/public/api/pages/:id', {id: '@_id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })

    .factory('FApp', function ($resource) {
        return $resource('http://localhost/projects/fbapi/public/api/apps/:id', {id: '@_id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })
    .factory('FPost', function ($resource) {
        return $resource('http://localhost/projects/fbapi/public/api/scrap/:id', {id: '@_id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })
    .factory('Post', function ($resource) {
        return $resource('http://localhost/projects/fbapi/public/api/posts/:id', {id: '@_id'}, {
            'update': {
                method: 'PUT'
            }
        });
    })
    .service('popupService', function ($window) {
        this.showPopup = function (message) {
            return $window.confirm(message);
        }
    });
