/**
 * Created by youssef on 3/5/2016.
 */
var fbapiApp = angular.module('fbapiApp',[
    'ui.router',
    'ngResource',
    'oc.lazyLoad',
    'toaster',
    'ngAnimate'
]);

fbapiApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);


fbapiApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
      // global setting
    };
    $rootScope.settings = settings;
    return settings;
}]);

fbapiApp.config(['$controllerProvider', function($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

fbapiApp.config(function($stateProvider,$httpProvider, $urlRouterProvider){
    //$urlRouterProvider.otherwise("htmls/index.html")

    $stateProvider.state('home',{
        url:'/',
        templateUrl:'htmls/index.html',
        data: {pageTitle: 'Home'},
        controller:'HomeCtrl',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'fbapiApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    files: [
                        // css files
                        'js/controllers/HomCtrl.js',
                    ]
                });
            }]
        }

    })

        //pages routing
        .state('pages',{
            url:'/pages',
            templateUrl:'htmls/pages/_index.html',
            data: {pageTitle: 'Pages'},
            controller:'PageCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'fbapiApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            // css files
                            'js/controllers/PageCtrl.js',
                        ]
                    });
                }]
            }
        })

        // Appps routing
        .state('apps',{
            url:'/apps',
            templateUrl:'htmls/apps/_index.html',
            data: {pageTitle: 'Apps'},
            controller:'FAppCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'fbapiApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            // css files
                            'js/controllers/FAppCtrl.js',
                        ]
                    });
                }]
            }
        })

        // Access token routing
        .state('access_tokens',{
            url:'/access_tokens',
            templateUrl:'htmls/access_tokens/_index.html',
            data: {pageTitle: 'Access Tokens'},
            controller:'ATokenCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'fbapiApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            // css files
                            'js/controllers/ATokenCtrl.js',
                        ]
                    });
                }]
            }
        })
        // Access token routing
        .state('scraping',{
            url:'/scraping',
            templateUrl:'htmls/scraping/_index.html',
            data: {pageTitle: 'Scraping'},
            controller:'ScrapCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'objectTable',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                // css files
                                'css/object-table-style.css',
                                'css/themes/blue-dust.css',
                                'js/lib/modules/object-table.js',
                            ]
                        },{
                        name: 'fbapiApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            // css files
                            'js/controllers/ScrapCtrl.js',
                        ]
                    }
                    ]);
                }]
            }
        })

        .state('posts',{
            url:'/posts',
            templateUrl:'htmls/posts/_index.html',
            data: {pageTitle: 'posts'},
            controller:'PostCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'fbapiApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            // css files
                            'js/controllers/PostCtrl.js',
                        ]
                    });
                }]
            }
        })

        .state('tasks',{
            url:'/tasks',
            templateUrl:'htmls/tasks/_index.html',
            data: {pageTitle: 'tasks'},
            controller:'TaskCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui-calendar',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'js/lib/modules/calendar.js'
                            ]
                        },
                        {
                            name: 'fbapiApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'css/fullcalendar.min.css',
                                'js/lib/moment.min.js',
                                'js/lib/fullcalendar.min.js',
                                'js/lib/jquery-ui.min.js',
                                'js/controllers/TaskCtrl.js',
                            ]
                        }
                    ]);
                }]
            }
        })

}).run(function($state){
    $state.go('access_tokens');
});
