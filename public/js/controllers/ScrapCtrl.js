/**
 * Created by youssef on 3/5/2016.
 */
angular.module('fbapiApp').controller('ScrapCtrl', function ($scope,toaster, FPost,Post,Page ) {
        $scope.load = false;
        $scope.show=false;
        $scope.success = false;
        $scope.error = false;
        $scope.pages=Page.query();
        $scope.startScrap = function () {
            toaster.pop('wait', "Scrape Posts", 'Wait Scraping ....',100000);
            $scope.load = true;
            $scope.error = false;
            $scope.data=FPost.query(function () {
                toaster.clear();
                toaster.pop('success', "Scrape Posts", 'Scrap Done');
                $scope.load = false;
                $scope.success = true;
            }, function (error) {
                console.log(error);
                toaster.pop('error', "Scrape Posts", 'Scrap error');
                $scope.load = false;
               // $scope.error = 'Unable to load data: ' + error.message;
            });
        };

        $scope.addPostToFav = function (item) {
            Post.save(item,function () {
                console.log("success");
                toaster.pop('success', "Add Post To Fav", 'Done ');
            }, function (error) {
                toaster.pop('error', "Add Post To Fav", 'error ');
                $scope.error = error.data;
            });
        };


        //console.log($scope.data);
    });

