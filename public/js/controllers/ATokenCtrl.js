/**
 * Created by youssef on 3/5/2016.
 */

angular.module('fbapiApp')
.controller('ATokenCtrl',function ($scope, $state,toaster,popupService, $window, $stateParams, Token, Page, FApp) {
    $scope.template = '';
    $scope.tokens = Token.query();
    $scope.error = false;


    $scope.createToken = function () {
        $scope.error = false;
        $scope.template = 'htmls/access_tokens/_create.html';
        $scope.token = new Token();
        $scope.pages = Page.query({to_post:true});
        $scope.apps = FApp.query();
        angular.element("#tokenModal").modal('show');
    };

    $scope.addToken = function () {
        $scope.token.$save(function () {
            angular.element("#tokenModal").modal('hide');
            $scope.reloadTokens();
            toaster.pop('success', "Create Token", 'Success', 5000, 'trustedHtml');
        }, function (error) {
            toaster.pop('error', "Create Token", 'error : crate Token', 5000, 'trustedHtml');
            $scope.error = error.data;
            console.log($scope.error.text[0]);
        });


    };

    $scope.editToken = function (token) {
        $scope.error = false;
        $scope.template = 'htmls/access_tokens/_edit.html';
        $scope.token = token;
        $scope.pages = Page.query();
        $scope.apps = FApp.query();
        angular.element("#tokenModal").modal('show');

    };

    $scope.updateToken = function () {
        Token.update({id: $scope.token.id}, $scope.token, function () {
            angular.element("#tokenModal").modal('hide');
            toaster.pop('success', "Edit Token", 'Success', 5000, 'trustedHtml');
            $scope.reloadTokens();
        }, function (error) {
            toaster.pop('error', "Edit Token", 'error : edit Token', 5000, 'trustedHtml');
            $scope.error = error.data;
        });
    }
    $scope.deleteToken = function (token) {
        if (popupService.showPopup('Really delete this?')) {
            Token.delete({id: token.id}, token, function () {
                toaster.pop('success', "Delete Token", 'Success', 5000, 'trustedHtml');
                $scope.reloadTokens();
            }, function (error) {
                toaster.pop('error', "Delete Token", 'error : Delete Token', 5000, 'trustedHtml');
                console.log(error);
            });
        }
    };
    $scope.reloadTokens = function () {
        $scope.tokens = Token.query();
    }

});

