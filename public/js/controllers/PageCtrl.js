/**
 * Created by youssef on 3/5/2016.
 */
angular.module('fbapiApp').controller('PageCtrl', function ($scope, $state, popupService, $window, $stateParams, Token, Page, FApp) {
        $scope.template = '';
        $scope.pages = Page.query();
        $scope.error = false;
        $scope.createPage = function () {
            $scope.error = false;
            $scope.template = 'htmls/pages/_create.html';
            $scope.page = new Page();
            angular.element("#tokenModal").modal('show');

        };

        $scope.addPage = function () {
            $scope.page.$save(function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPages();
            }, function (error) {
                $scope.error = error.data;
            });
        };

        $scope.editPage = function (page) {
            $scope.error = false;
            $scope.template = 'htmls/pages/_edit.html';
            $scope.page = page;
            angular.element("#tokenModal").modal('show');

        };

        $scope.updatePage = function () {
            Page.update({id: $scope.page.id}, $scope.page, function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPages();
            }, function (error) {
                $scope.error = error.data;
            });
        };
        $scope.deletePage = function (page) {
            if (popupService.showPopup('Really delete this?')) {
                Page.delete({id: page.id}, page, function () {
                    $scope.reloadPages();
                }, function (error) {
                    console.log(error);
                });
            }
        };
        $scope.reloadPages = function () {
            $scope.pages = Page.query();
        };

    })

