/**
 * Created by youssef on 3/5/2016.
 */
angular.module('fbapiApp').controller('PostCtrl', function ($scope, $state, popupService, $window, $stateParams, Post, Page) {
        $scope.template = '';
        $scope.statu=0;
        $scope.posts = Post.query();
        $scope.error = false;

        $scope.createPost = function () {
            $scope.error = false;
            $scope.pages = Page.query();
            $scope.template = 'htmls/posts/_create.html';
            $scope.post = new Post();
            angular.element("#tokenModal").modal('show');

        };

        $scope.addPost = function () {
            $scope.post.$save(function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPosts();
            }, function (error) {
                $scope.error = error.data;
            });
        };

        $scope.editPost = function (post) {
            $scope.pages = Page.query({to_post:true});
            $scope.error = false;
            $scope.template = 'htmls/posts/_edit.html';
            $scope.post = post;
            angular.element("#tokenModal").modal('show');

        };

        $scope.updatePost = function () {
            $scope.post.status=1;
            Post.update({id: $scope.post.id}, $scope.post, function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPosts();
            }, function (error) {
                $scope.error = error.data;
            });
        };
        $scope.deletePost = function (post) {
            if (popupService.showPopup('Really delete this?')) {
                Post.delete({id: post.id}, post, function () {
                    $scope.reloadPosts();
                }, function (error) {
                    console.log(error);
                });
            }
        };

        $scope.reloadPosts = function () {
            $scope.posts = Post.query();
        };

        function timefilter($scope, dateFilter) {
            $scope.post.time_to_post = new Date();

            $scope.$watch('datetime-local', function (date)
            {
                $scope.dateString = dateFilter(date, 'yyyy-MM-dd');
            });

            $scope.$watch('post.time_to_post', function (dateString)
            {
                $scope.post.time_to_post = new Date(dateString);
            });
        };

    $scope.setStatus= function (id) {
        $scope.statu=id;
    }

    })

