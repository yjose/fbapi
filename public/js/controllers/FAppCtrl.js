/**
 * Created by youssef on 3/5/2016.
 */
angular.module('fbapiApp').controller('FAppCtrl', function ($scope, $state, popupService, $window, $stateParams, Token, Page, FApp) {
        $scope.template = '';
        $scope.apps = FApp.query();
        $scope.error = false;
        $scope.createApp = function () {
            $scope.error = false;
            $scope.template = 'htmls/apps/_create.html';
            $scope.app = new FApp();
            angular.element("#tokenModal").modal('show');

        };

        $scope.addApp = function () {
            $scope.app.$save(function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadApps();
            }, function (error) {
                $scope.error = error.data;
            });
        };

        $scope.editApp = function (app) {
            $scope.error = false;
            $scope.template = 'htmls/apps/_edit.html';
            $scope.app = app;
            angular.element("#tokenModal").modal('show');

        };

        $scope.updateApp = function () {
            FApp.update({id: $scope.app.id}, $scope.app, function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadApps();
            }, function (error) {
                $scope.error = error.data;
            });
        };
        $scope.deleteApp = function (app) {
            if (popupService.showPopup('Really delete this?')) {
                FApp.delete({id: app.id}, app, function () {
                    $scope.reloadApps();
                }, function (error) {
                    console.log(error);
                });
            }
        };
        $scope.reloadApps = function () {
            $scope.apps = FApp.query();
        };

    })

