/**
 * Created by youssef on 3/5/2016.
 */

angular.module('fbapiApp').controller('TaskCtrl', function ($scope, $compile, $timeout, uiCalendarConfig, Post) {

    Post.query({status:0,format: 'true'}, function (posts) {
        //console.log(posts);
        $scope.initPostEvents(posts);
    });


    $scope.eventSources = [ $scope.events ];
    $scope.events=[];
    /* event source that contains custom events on the scope */
    Post.query({status:4,format:'true'},function(posts){
        angular.forEach(posts, function (postt, key) {
            var post = $.extend({}, postt);
            post.start =$.fullCalendar.moment(post.time_to_post);
            post.stick=true;
            post.source1=post.source;
            post.source=null;
            post.backgroundColor=post.background;
            $scope.events.push(post);
        });
        //console.log($scope.events);
    });

    $scope.initEvents=function(){
        console.log($scope.events);
        angular.forEach($scope.events, function (post, key) {
            $('#calendar').fullCalendar('renderEvent', post, true);
        });
    }


    var initDrag = function (el, post) {
        post.title = 'Post #' + post.id;
        post.stick = true;
        post.allDay = false;
        post.source1=post.source;
        post.source=null;
        post.backgroundColor = post.background;
        el.data('eventPost', post);
        el.draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0 //  original position after the drag
        });
    };

    var addEvent = function (post) {
        id = post.id.length === 0 ? 0 : post.id;
        var html = $('<div class="external-event label label-default" style="background-color: ' + post.background + '"> Post #' + id + '</div>');
        jQuery('#event_box').append(html);
        initDrag(html, post);
    };
    $scope.initPostEvents = function (posts) {
        $('#event_box').html("");
        angular.forEach(posts, function (post, key) {
            addEvent(post);
        });

    }

    $scope.dropAction = function (date, allDay) {
        var copiedEventObject = $(this).data('eventPost');
        //var copiedEventObject = $.extend({}, originalEventObject);
        copiedEventObject.start = date;
        $('#calendar').fullCalendar('renderEvent', copiedEventObject);
        console.log(copiedEventObject);
        $scope.addEventToPost(copiedEventObject);
        $(this).remove();

    };

    $scope.removeEvent = function (event, jsEvent, ui, view) {
        if (isEventOverDiv(jsEvent.clientX, jsEvent.clientY)) {
            console.log('ifremoveEvent');
            $('#calendar').fullCalendar('removeEvents', event._id);
            addEvent(event);
            event.status=0;
            $scope.eventsToPost.push(event);
        }
        console.log('removeEvent');
    };


    var isEventOverDiv = function (x, y) {
        console.log(x);
        console.log(y);
        var external_events = $('#drag');
        var offset = external_events.offset();
        console.log(offset);
        if(y<offset.top || x<offset.left)
           return true;
        return false;
    };
    $scope.editeEvent=function( event, delta, revertFunc, jsEvent, ui, view ) {
    $scope.addEventToPost(event);
        console.log('EditAction');
    };

    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 650,
            editable: true,
            droppable: true,
            dragRevertDuration: 0,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            drop: $scope.dropAction,
            eventRender: $scope.eventRender,
            eventDrop:$scope.editeEvent,
            eventDragStop: $scope.removeEvent

        }
    };




    /* event sources array*/
    $scope.eventSources = [$scope.events];
    //$scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];



    // event to do
    $scope.eventsToPost = [];
    $scope.addEventToPost=function(post){
        post.time_to_post=post.start._d;
        post.status=4;
        post.source=post.source1;
        //p={id:post.id,status:4,time_to_post:};
        var result = $scope.eventsToPost.filter(function (obj) {return obj.id === post.id;})[0];
        if(result!=null){
            result.time_to_post =post.start._d;
            result.status=4;
            result.source=result.source1;
        }
        else
           $scope.eventsToPost.push(post);
          console.log($scope.eventsToPost);
    }

    $scope.savePosts=function(){
        angular.forEach($scope.eventsToPost, function (post, key) {
            Post.update({id: post.id},post, function () {
                console.log(post);
                console.log('succes');
            }, function (error) {
                console.log(error);
            });
        });
    };

});

