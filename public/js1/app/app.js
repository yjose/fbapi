/**
 * Created by Sandeep on 01/06/14.
 */

var fbapiApp = angular.module('fbapiApp',[
    'ui.router',
    'ngResource',
    "oc.lazyLoad", 
    ]);

fbapiApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

fbapiApp.config(function($stateProvider,$httpProvider){
    $stateProvider.state('home',{
        url:'/',
        templateUrl:'htmls/index.html',
        controller:'HomeController',
       
    })

    //pages routing  
    .state('pages',{
        url:'/pages',
        templateUrl:'htmls/pages/_index.html',
        controller:'PageController'
    })

     // Appps routing 
    .state('apps',{
        url:'/apps',
        templateUrl:'htmls/apps/_index.html',
        controller:'FAppController'
    })

    // Access token routing 
    .state('access_tokens',{
        url:'/access_tokens',
        templateUrl:'htmls/access_tokens/_index.html',
        controller:'TokenController'
    })
        // Access token routing
    .state('scraping',{
            url:'/scraping',
            templateUrl:'htmls/scraping/_index.html',
            controller:'ScrapController'
    })

    .state('posts',{
            url:'/posts',
            templateUrl:'htmls/posts/_index.html',
            controller:'PostController'
    })
}).run(function($state){
   $state.go('access_tokens');
});