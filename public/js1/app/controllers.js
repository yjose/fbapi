/**
 * Created by Sandeep on 01/06/14.
 */
angular.module('fbapiApp.controllers', ['objectTable'])
    // Token Controller
    .controller('TokenController', function ($scope, $state, popupService, $window, $stateParams, Token, Page, FApp) {
        $scope.template = '';
        $scope.tokens = Token.query();
        $scope.error = false;
        $scope.createToken = function () {
            $scope.error = false;
            $scope.template = 'htmls/access_tokens/_create.html';
            $scope.token = new Token();
            $scope.pages = Page.query();
            $scope.apps = FApp.query();
            angular.element("#tokenModal").modal('show');

        };

        $scope.addToken = function () {
            $scope.token.$save(function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadTokens();
            }, function (error) {
                console.log("erreur de add");
                $scope.error = error.data;
                console.log($scope.error.text[0]);
            });


        };

        $scope.editToken = function (token) {
            $scope.error = false;
            $scope.template = 'htmls/access_tokens/_edit.html';
            $scope.token = token;
            $scope.pages = Page.query();
            $scope.apps = FApp.query();
            angular.element("#tokenModal").modal('show');

        };

        $scope.updateToken = function () {
            Token.update({id: $scope.token.id}, $scope.token, function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadTokens();
            }, function (error) {
                $scope.error = error.data;
            });
        }
        $scope.deleteToken = function (token) {
            if (popupService.showPopup('Really delete this?')) {
                Token.delete({id: token.id}, token, function () {
                    $scope.reloadTokens();
                }, function (error) {
                    console.log(error);
                });
            }
        };
        $scope.reloadTokens = function () {
            $scope.tokens = Token.query();
        }

    })
// Page Controller
    .controller('PageController', function ($scope, $state, popupService, $window, $stateParams, Token, Page, FApp) {
        $scope.template = '';
        $scope.pages = Page.query();
        $scope.error = false;
        $scope.createPage = function () {
            $scope.error = false;
            $scope.template = 'htmls/pages/_create.html';
            $scope.page = new Page();
            angular.element("#tokenModal").modal('show');

        };

        $scope.addPage = function () {
            $scope.page.$save(function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPages();
            }, function (error) {
                $scope.error = error.data;
            });
        };

        $scope.editPage = function (page) {
            $scope.error = false;
            $scope.template = 'htmls/pages/_edit.html';
            $scope.page = page;
            angular.element("#tokenModal").modal('show');

        };

        $scope.updatePage = function () {
            Page.update({id: $scope.page.id}, $scope.page, function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPages();
            }, function (error) {
                $scope.error = error.data;
            });
        };
        $scope.deletePage = function (page) {
            if (popupService.showPopup('Really delete this?')) {
                Page.delete({id: page.id}, page, function () {
                    $scope.reloadPages();
                }, function (error) {
                    console.log(error);
                });
            }
        };
        $scope.reloadPages = function () {
            $scope.pages = Page.query();
        };

    })

    // Post Controller
    .controller('PostController', function ($scope, $state, popupService, $window, $stateParams, Post, Page) {
        $scope.template = '';
        $scope.posts = Post.query();
        $scope.error = false;

        $scope.createPost = function () {
            $scope.error = false;
            $scope.pages = Page.query();
            $scope.template = 'htmls/posts/_create.html';
            $scope.post = new Post();
            angular.element("#tokenModal").modal('show');

        };

        $scope.addPost = function () {
            $scope.post.$save(function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPosts();
            }, function (error) {
                $scope.error = error.data;
            });
        };

        $scope.editPost = function (post) {
            $scope.pages = Page.query();
            $scope.error = false;
            $scope.template = 'htmls/posts/_edit.html';
            $scope.post = post;
            angular.element("#tokenModal").modal('show');

        };

        $scope.updatePost = function () {
            $scope.post.status=1;
            Post.update({id: $scope.post.id}, $scope.post, function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadPosts();
            }, function (error) {
                $scope.error = error.data;
            });
        };
        $scope.deletePost = function (post) {
            if (popupService.showPopup('Really delete this?')) {
                Post.delete({id: post.id}, post, function () {
                    $scope.reloadPosts();
                }, function (error) {
                    console.log(error);
                });
            }
        };

        $scope.reloadPosts = function () {
            $scope.posts = Post.query();
        };

        function timefilter($scope, dateFilter) {
            $scope.post.time_to_post = new Date();

            $scope.$watch('datetime-local', function (date)
            {
                $scope.dateString = dateFilter(date, 'yyyy-MM-dd');
            });

            $scope.$watch('post.time_to_post', function (dateString)
            {
                $scope.post.time_to_post = new Date(dateString);
            });
        };

    })
// APP Controller
    .controller('FAppController', function ($scope, $state, popupService, $window, $stateParams, Token, Page, FApp) {
        $scope.template = '';
        $scope.apps = FApp.query();
        $scope.error = false;
        $scope.createApp = function () {
            $scope.error = false;
            $scope.template = 'htmls/apps/_create.html';
            $scope.app = new FApp();
            angular.element("#tokenModal").modal('show');

        };

        $scope.addApp = function () {
            $scope.app.$save(function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadApps();
            }, function (error) {
                $scope.error = error.data;
            });
        };

        $scope.editApp = function (app) {
            $scope.error = false;
            $scope.template = 'htmls/apps/_edit.html';
            $scope.app = app;
            angular.element("#tokenModal").modal('show');

        };

        $scope.updateApp = function () {
            FApp.update({id: $scope.app.id}, $scope.app, function () {
                angular.element("#tokenModal").modal('hide');
                $scope.reloadApps();
            }, function (error) {
                $scope.error = error.data;
            });
        };
        $scope.deleteApp = function (app) {
            if (popupService.showPopup('Really delete this?')) {
                FApp.delete({id: app.id}, app, function () {
                    $scope.reloadApps();
                }, function (error) {
                    console.log(error);
                });
            }
        };
        $scope.reloadApps = function () {
            $scope.apps = FApp.query();
        };

    })


    //scrapControllers
.controller('ScrapController', function ($scope, FPost,Post,Page ) {

        $scope.load = false;
        $scope.show=false;
        $scope.success = false;
        $scope.error = false;
        $scope.pages=Page.query();
        $scope.startScrap = function () {
            $scope.load = true;
            $scope.error = false;
            $scope.data=FPost.query(function () {
                $scope.load = false;
                $scope.success = true;
            }, function (error) {
                console.log(error);
                $scope.load = false;
                $scope.error = 'Unable to load data: ' + error.message;
            });
        };

        $scope.addPostToFav = function (item) {
            Post.save(item,function () {
                console.log("success");
            }, function (error) {
                $scope.error = error.data;
            });
        };


        //console.log($scope.data);
    });
