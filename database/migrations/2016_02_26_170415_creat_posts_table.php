<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->string('type');
            $table->text('message');
            $table->text('description');
            $table->text('source');
            $table->text('link');
            $table->integer('status')->default(0);
            $table->string('success_id')->default(0);
            $table->dateTime('time_to_post');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
